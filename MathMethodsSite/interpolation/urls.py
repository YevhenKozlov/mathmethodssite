from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^$', views.page),
    url(r'newton', include('MathMethodsSite.newton.urls')),
    url(r'lagrange', include('MathMethodsSite.lagrange.urls')),
]