from django.apps import AppConfig


class InterpolationConfig(AppConfig):
    name = 'MathMethodsSite.interpolation'
