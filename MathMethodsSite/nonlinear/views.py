from django.shortcuts import render
from .models import bisection_method, chord_method, combination_method, iteration_method, newton_method, parse_expr

accuracy_graphic = 500


def page(request):
    if request.POST:
        try:
            function = parse_expr(request.POST['function'])
            a = float(request.POST['a'])
            b = float(request.POST['b'])
            if a >= b:
                raise Exception()
            E = float(request.POST['E'])
            X = None
            log = ''
            if request.POST['method'] == 'bisection_method':
                X, log = bisection_method(function, a, b, E)
            elif request.POST['method'] == 'chord_method':
                X, log = chord_method(function, a, b, E)
            elif request.POST['method'] == 'combination_method':
                X, log = combination_method(function, a, b, E)
            elif request.POST['method'] == 'iteration_method':
                X, log = iteration_method(function, a, b, E)
            elif request.POST['method'] == 'newton_method':
                X, log = newton_method(function, a, b, E)
            coef = (b - a) / accuracy_graphic
            coordinates = str([[a + coef * i, function.subs('x', a + coef * i).evalf()] for i in range(accuracy_graphic + 1)])
            return render(request, 'nonlinear.html', {'error': False, 'coordinates': coordinates, 'log': log, 'X': str(X), 'function': request.POST['function'], 'a': request.POST['a'], 'b': request.POST['b'], 'E': request.POST['E'], 'method': request.POST['method']})
        except:
            return render(request, 'nonlinear.html', {'error': True, 'function': request.POST['function'], 'a': request.POST['a'], 'b': request.POST['b'], 'E': request.POST['E'], 'method': request.POST['method']})
    return render(request, 'nonlinear.html', {'error': False})