from django.apps import AppConfig


class NonlinearConfig(AppConfig):
    name = 'MathMethodsSite.nonlinear'
