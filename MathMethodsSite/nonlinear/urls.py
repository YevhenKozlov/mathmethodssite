from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^$', views.page),
    url(r'rules', include('MathMethodsSite.rules.urls')),
]