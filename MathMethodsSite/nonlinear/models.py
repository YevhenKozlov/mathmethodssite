from sympy.parsing.sympy_parser import parse_expr
from sympy import diff
import math


def bisection_method(function, a, b, E):
    log = '<i><p>'
    result = None
    if function.subs('x', a).evalf() * function.subs('x', b).evalf() < 0:
        for iter in range(10**4):
            x = (b + a) / 2
            log += 'Ітерація:  <b>%r</b><br>a = %r, b = %r, <b>x = %r</b>' % (iter, a, b, x) + '<br>'
            if math.fabs(b - a) < E:
                log += '<b>Розв\'язок отримав необхідну точність!</b>' + '</p>'
                result = x
                break
            if function.subs('x', x).evalf() == 0.0:
                log += '<b>Абсолютний розв\'язок!</b>' + '</p>'
                result = x
                break
            if function.subs('x', a).evalf() * function.subs('x', x).evalf() < 0:
                b = x
                log += 'x -> b' + '</p><p>'
            else:
                a = x
                log += 'x -> a' + '</p><p>'
            if iter == 9999:
                log += '<b>Перевищена допустима кількість ітерацій!</b>' + '</p>'
                result = 'Перевищена допустима кількість ітерацій!'
    else:
        raise Exception()
    return result, log + '</i>'


def chord_method(function, a, b, E):
    log = '<i><p>'
    result = None
    if function.subs('x', a).evalf() * function.subs('x', b).evalf() < 0:
        for iter in range(10**4):
            x = float(a - (function.subs('x', a).evalf() * (b - a)) / (function.subs('x', b).evalf() - function.subs('x', a).evalf()))
            log += 'Ітерація:  <b>%r</b><br>a = %r, b = %r, <b>x = %r</b>' % (iter, a, b, x) + '<br>'
            if math.fabs(b - a) < E:
                log += '<b>Розв\'язок отримав необхідну точність!</b>' + '</p>'
                result = x
                break
            if function.subs('x', x).evalf() == 0.0:
                log += '<b>Абсолютний розв\'язок!</b>' + '</p>'
                result = x
                break
            if function.subs('x', a).evalf() * function.subs('x', x).evalf() < 0:
                b = x
                log += 'x -> b' + '</p><p>'
            else:
                a = x
                log += 'x -> a' + '</p><p>'
            if iter == 9999:
                log += '<b>Перевищена допустима кількість ітерацій!</b>' + '</p>'
                result = 'Перевищена допустима кількість ітерацій!'
    else:
        raise Exception()
    return result, log + '</i>'


def newton_method(function, a, b, E):
    log = '<i><p>'
    result = None
    if function.subs('x', a).evalf() * function.subs('x', b).evalf() < 0:
        for iter in range(10**4):
            x = float(b - function.subs('x', b).evalf() / diff(function).subs('x', b).evalf())
            log += 'Ітерація:  <b>%r</b><br>a = %r, b = %r, <b>x = %r</b>' % (iter, a, b, x) + '<br>'
            if math.fabs(x - b) < E:
                log += '<b>Розв\'язок отримав необхідну точність!</b>' + '</p>'
                result = x
                break
            if function.subs('x', x).evalf() == 0.0:
                log += '<b>Абсолютний розв\'язок!</b>' + '</p>'
                result = x
                break
            if math.fabs(math.fabs(x - a) + math.fabs(x - b) - math.fabs(a - b)) >= 0.00001:
                a, b = b, a
                log += 'a <=> b' + '</p><p>'
            else:
                b = x
                log += 'x -> b' + '</p><p>'
            if iter == 9999:
                log += '<b>Перевищена допустима кількість ітерацій!</b>' + '</p>'
                result = 'Перевищена допустима кількість ітерацій!'
    else:
        raise Exception()
    return result, log + '</i>'


def combination_method(function, a, b, E):
    log = '<i><p>'
    result = None
    if function.subs('x', a).evalf() * function.subs('x', b).evalf() < 0:
        for iter in range(10**4):
            a = float(a - (function.subs('x', a).evalf() * (b - a)) / (function.subs('x', b).evalf() - function.subs('x', a).evalf()))
            b = float(b - function.subs('x', b).evalf() / diff(function).subs('x', b).evalf())
            x = (a + b) / 2
            log += 'Ітерація:  <b>%r</b><br>a = %r, b = %r, <b>x = %r</b>' % (iter, a, b, x)
            if math.fabs(a - b) < E:
                log += '<br><b>Розв\'язок отримав необхідну точність!</b>' + '</p>'
                result = x
                break
            if function.subs('x', x).evalf() == 0.0:
                log += '<br><b>Абсолютний розв\'язок!</b>' + '</p>'
                result = x
                break
            if iter == 9999:
                log += '<br><b>Перевищена допустима кількість ітерацій!</b>' + '</p>'
                result = 'Перевищена допустима кількість ітерацій!'
            log += '</p><p>'
    else:
        raise Exception()
    return result, log + '</i>'


def iteration_method(function, a, b, E):
    log = '<i><p>'
    result = None
    if function.subs('x', a).evalf() * function.subs('x', b).evalf() < 0:
        x = (b + a) / 2
        l = 1 / diff(function).subs('x', x).evalf()
        new_function = parse_expr('x - %r * (%r)' % (l, function))
        for iter in range(10**4):
            y = new_function.subs('x', x).evalf()
            log += 'Ітерація:  <b>%r</b><br>a = %r, b = %r, <b>x = %r</b>' % (iter, a, b, y)
            if math.fabs(y - x) < E:
                log += '<br><b>Розв\'язок отримав необхідну точність!</b>' + '</p>'
                result = y
                break
            if function.subs('x', y).evalf() == 0.0:
                log += '<br><b>Абсолютний розв\'язок!</b>' + '</p>'
                result = y
                break
            if iter == 9999:
                log += '<br><b>Перевищена допустима кількість ітерацій!</b>' + '</p>'
                result = 'Перевищена допустима кількість ітерацій!'
            x = y
            log += '</p><p>'
    else:
        raise Exception()
    return result, log + '</i>'