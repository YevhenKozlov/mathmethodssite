def lagrange_method(x, list_x, list_y):
    size = len(list_x)
    y = 0
    for i in range(size):
        numerator = denumerator = 1
        for j in range(size):
            if i != j:
                numerator *= x - list_x[j]
                denumerator *= list_x[i] - list_x[j]
        y += list_y[i] * (numerator / denumerator)
    return y