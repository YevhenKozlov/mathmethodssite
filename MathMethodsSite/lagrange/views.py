from django.shortcuts import render
from .models import lagrange_method

# Create your views here.
def page(request):
    if request.POST:
        try:
            list_x = [float(number) for number in request.POST['list_x'].split(' ')]
            list_y = [float(number) for number in request.POST['list_y'].split(' ')]
            for elem in list_x:
                if list_x.count(elem) > 1:
                    raise Exception()
            for i in range(len(list_x) - 1):
                if list_x[i] >= list_x[i + 1]:
                    raise Exception()
            if len(list_y) != len(list_x):
                raise Exception()
            X = float(request.POST['X'])
            Y = lagrange_method(X, list_x, list_y)
            coordinates = [[list_x[i], list_y[i]] for i in range(len(list_x))]
            for i in range(len(coordinates)):
                if coordinates[i][0] >= X:
                    coordinates.insert(i, [X, Y])
                    break
                if i + 1 == len(coordinates):
                    coordinates.append([X, Y])
            coordinates = str(coordinates)
            return render(request, 'lagrange.html', {'error': False, 'list_x_str': request.POST['list_x'], 'list_y_str': request.POST['list_y'], 'X': str(X), 'Y': str(Y), 'coordinates': coordinates})
        except:
            return render(request, 'lagrange.html', {'error': True, 'list_x_str': request.POST['list_x'], 'list_y_str': request.POST['list_y'],'X': request.POST['X']})
    else:
        return render(request, 'lagrange.html', {'error': False})