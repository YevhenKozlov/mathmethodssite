from django.apps import AppConfig


class LagrangeConfig(AppConfig):
    name = 'MathMethodsSite.lagrange'
