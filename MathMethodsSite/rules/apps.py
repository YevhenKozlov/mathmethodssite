from django.apps import AppConfig


class RulesConfig(AppConfig):
    name = 'MathMethodsSite.rules'
