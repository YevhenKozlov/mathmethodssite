from django.apps import AppConfig


class NewtonConfig(AppConfig):
    name = 'MathMethodsSite.newton'
