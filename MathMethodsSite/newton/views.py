from django.shortcuts import render
from .models import newton_method

# Create your views here.
def page(request):
    if request.POST:
        try:
            list_y = [float(number) for number in request.POST['list_y'].split(' ')]
            start_x = float(request.POST['start_x'])
            h = float(request.POST['h'])
            X = float(request.POST['X'])
            if h <= 0:
                raise Exception()
            Y = newton_method(X, list_y, start_x, h)
            coordinates = [[start_x + h * i, list_y[i]] for i in range(len(list_y))]
            for i in range(len(coordinates)):
                if coordinates[i][0] >= X:
                    coordinates.insert(i, [X, Y])
                    break
                if i + 1 == len(coordinates):
                    coordinates.append([X, Y])
            coordinates = str(coordinates)
            return render(request, 'newton.html', {'error': False, 'list_y_str': request.POST['list_y'], 'Y': str(Y), 'X': str(X), 'h': str(h), 'start_x': str(start_x), 'coordinates': coordinates})
        except:
            return render(request, 'newton.html', {'error': True, 'list_y_str': request.POST['list_y'], 'X': request.POST['X'], 'h': request.POST['h'], 'start_x': request.POST['start_x']})
    else:
        return render(request, 'newton.html', {'error': False})