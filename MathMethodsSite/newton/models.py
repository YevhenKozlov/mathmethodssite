def newton_method(x, list_y, start_x, h):
    power = len(list_y)
    q = (x - start_x) / h
    list_d = list_y.copy()
    t = 1
    y = list_d[0]
    for iter in range(1, power):
        for i in range(power - iter):
            list_d[i] = list_d[i + 1] - list_d[i]
        t *= (q - iter + 1) / iter
        r = list_d[0] * t
        y += r
    return y
